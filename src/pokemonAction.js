// export const listPokemens = async (dispatch) => {
//     try {
//         dispatch({ type: "POKEMON_LIST_REQUEST" });
//         const api = await fetch(`http://localhost:3000/pokes/list-pokemons`);
//         const data = await api.json();

//         dispatch({
//             type: "POKEMON_LIST_SUCCESS",
//             payload: data,
//         });
//     } catch (error) {
//         dispatch({
//             type: "POKEMON_LIST_FAIL",
//             payload:
//                 error.response && error.response.data.message
//                     ? error.response.data.message
//                     : error.message,
//         });
//     }
// };

export const listPokemens = () => {
    return async (dispatch) => {
        try {
            dispatch({ type: "POKEMON_LIST_REQUEST" });
            const api = await fetch(
                `http://localhost:3000/pokes/list-pokemons`
            );
            const data = await api.json();

            dispatch({
                type: "POKEMON_LIST_SUCCESS",
                payload: data,
            });
        } catch (error) {
            dispatch({
                type: "POKEMON_LIST_FAIL",
                payload:
                    error.response && error.response.data.message
                        ? error.response.data.message
                        : error.message,
            });
        }
    };
};
