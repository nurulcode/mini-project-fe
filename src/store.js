import { applyMiddleware, combineReducers, createStore } from "redux";
import { thunk } from 'redux-thunk'

export const pokemenList = (state = { pokemonsTer: [] }, action) => {
    switch (action.type) {
        case "POKEMON_LIST_REQUEST":
            return { loading: true, pokemonsTer: [] };
        case "POKEMON_LIST_SUCCESS":
            return { loading: false, pokemonsTer: action.payload };
        case "POKEMON_LIST_FAIL":
            return { loading: false, error: action.payload };
        default:
            return state;
    }
};

const reducer = combineReducers({
    pokemenList: pokemenList,
});

// Buat store Redux dari reducer
const store = createStore(reducer, applyMiddleware(thunk));

export default store;
