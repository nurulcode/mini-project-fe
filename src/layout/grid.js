import React from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

const Grid = ({ pokemons, next, status }) => {
    const handleButton = () => {
        next();
    };

    return (
        <div>
            <div>
                <Row>
                    {pokemons.map((poke, index) => (
                        <Col md={3} key={index}>
                            <Card
                                className="my-3 p-3 rounded"
                                key={poke.name}
                                style={{ width: "18rem" }}
                            >
                                <Card.Img
                                    variant="top"
                                    src={poke.sprites.front_default}
                                    alt={poke.name}
                                />
                                <Card.Body>
                                    <Card.Title>{poke.name}</Card.Title>
                                    <Card.Text>
                                        Pokemon details here...
                                        <LinkContainer
                                            to={`/pokemon/${poke.id}/${status}`}
                                        >
                                            <Button
                                                variant="light"
                                                className="btn-sm"
                                            >
                                                Details
                                            </Button>
                                        </LinkContainer>
                                    </Card.Text>
                                </Card.Body>
                            </Card>
                        </Col>
                    ))}
                </Row>
            </div>
            {pokemons.length >= 20 && (
                <div>
                    <Button variant="primary" onClick={handleButton}>
                        Show more
                    </Button>
                </div>
            )}
        </div>
    );
};

export default Grid;
