import React from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

const GridA = (pokemons) => {
    return (
        <div>
            <div>
                <Row>
                    {pokemons.pokemons.data ? (
                        pokemons.pokemons.data.map((poke, index) => (
                            <Col md={3} key={index}>
                                <Card
                                    className="my-3 p-3 rounded"
                                    key={poke.name}
                                    style={{ width: "18rem" }}
                                >
                                    <Card.Img
                                        variant="top"
                                        src={poke.content}
                                        alt={poke.name}
                                    />
                                    <Card.Body>
                                        <Card.Title>{poke.name}</Card.Title>
                                        <Card.Text>
                                            Pokemon details here...
                                            <LinkContainer
                                                to={`/pokemon/${
                                                    poke.name.split("-")[0]
                                                }/${poke.name}`}
                                            >
                                                <Button
                                                    variant="light"
                                                    className="btn-sm"
                                                >
                                                    Details
                                                </Button>
                                            </LinkContainer>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        ))
                    ) : (
                        <h5>Belum punya pokemon</h5>
                    )}
                </Row>
            </div>
        </div>
    );
};

export default GridA;
