import { useEffect, useState } from "react";
import { Button, Container } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import GridA from "../layout/card";
import { useDispatch, useSelector } from "react-redux";
import { listPokemens } from "../pokemonAction";

const MyPokemonScreen = ({ match }) => {
    const dispatch = useDispatch();

    const [pokemons, setPokemons] = useState([]);
    const [notFound, setNotFound] = useState(false);
    const [searching, setSearching] = useState(false);

    const pokemenList = useSelector((state) => state.pokemenList);
    const { pokemonsTer } = pokemenList 
    

    const showPokemons = async (limit = 10, offset = 0) => {
        const api = await fetch(`http://localhost:3000/pokes/list-pokemons`);

        const { data } = await api.json();
        setPokemons(data);
    };

    useEffect(() => {
        dispatch(listPokemens());

        if (!searching) {
            showPokemons();
        }
    }, [dispatch]);

    return (
        <>
            <Container className="p-3">
                <LinkContainer to={`/`}>
                    <Button variant="light" className="btn-sm">
                        HOME
                    </Button>
                </LinkContainer>
                {notFound ? (
                    <div>'Pokemon not found'</div>
                ) : (
                    <GridA pokemons={pokemonsTer} status="me" />
                )}
            </Container>
        </>
    );
};

export default MyPokemonScreen;
