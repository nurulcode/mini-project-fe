import { useEffect, useState } from "react";
import { Button, Container } from "react-bootstrap";
import Grid from "../layout/grid";
import { LinkContainer } from "react-router-bootstrap";

const HomeScreen = ({ match }) => {
    const [pokemons, setPokemons] = useState([]);
    const [total, setTotal] = useState(0);
    const [notFound, setNotFound] = useState(false);
    const [search, setSearch] = useState([]);
    const [searching, setSearching] = useState(false);

    // const handleSearch = async (textSearch) => {
    //     if (!textSearch) {
    //         setSearch([]);
    //         setNotFound(false);
    //         return;
    //     }

    //     setSearching(true);
    //     try {
    //         const api = await fetch(
    //             `https://pokeapi.co/api/v2/pokemon/${textSearch.toLowerCase()}`
    //         );
    //         const data = await api.json();
    //         setSearch([data]);
    //         setNotFound(false);
    //     } catch (error) {
    //         setSearch([]);
    //         setNotFound(true);
    //     } finally {
    //         setSearching(false);
    //     }
    // };

    const showPokemons = async (limit = 10, offset = 0) => {
        const api = await fetch(
            `https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`
        );
        const data = await api.json();

        const promises = data.results.map(async (pokemon) => {
            const result = await fetch(pokemon.url);
            const res = await result.json();
            return res;
        });

        try {
            const results = await Promise.all(promises);
            setSearch([]);
            setPokemons((prevPokemons) => [...prevPokemons, ...results]);
            setNotFound(false);
            setTotal((prevTotal) => prevTotal + results.length);
        } catch (error) {
            console.error("Error fetching Pokemon:", error);
        }
    };

    const nextPokemon = () => {
        showPokemons(10, total);
    };

    useEffect(() => {
        if (!searching) {
            showPokemons();
        }
    }, [searching]);

    const poke = search.length > 0 ? search : pokemons;

    return (
        <>
            <Container className="p-3">
                <LinkContainer to={`/pokemon-me`}>
                    <Button variant="light" className="btn-sm">
                        MY POKECHAN
                    </Button>
                </LinkContainer>
                {notFound ? (
                    <div>'Pokemon not found'</div>
                ) : (
                    <Grid pokemons={poke} next={nextPokemon} status="global" />
                )}
            </Container>
        </>
    );
};

export default HomeScreen;
