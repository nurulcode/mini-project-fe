import React, { useEffect, useState } from "react";
import { Container, Row, Col, Image, Button } from "react-bootstrap";

const PokemonScreen = ({ pokemon, show, onHide, match, status, history }) => {
    const pokemonId = match.params.id;
    const pokemonstatus = match.params.status;
    const [pokemons, setPokemons] = useState([]);
    const [savedPokemons, setSavedPokemons] = useState([]);
    

    const showPokemons = async (limit = 10, offset = 0) => {
        const api = await fetch(
            `https://pokeapi.co/api/v2/pokemon/${pokemonId}`
        );
        const data = await api.json();
        setPokemons(data);
    };

    const handleSavePokemon = async () => {
        try {
            const isPokemonExists = savedPokemons.some(
                (pokemon) => pokemon.id === pokemons.id
            );

            if (!isPokemonExists) {
                fetch("http://localhost:3000/pokes/save-pokemon", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        name: `${pokemons.name}`,
                        content: pokemons.sprites?.front_default,
                        version: 0,
                    }),
                })
                    .then((response) => {
                        if (!response.ok) {
                            throw new Error("Network response was not ok.");
                        }
                        return response.json(); // Mengambil data respons dalam bentuk JSON
                    })
                    .then((data) => {
                        if (data.data.status === "redirect") {
                            history.push("/");
                        } else {
                            alert(
                                `Belum bisa tambah pokemon nih, persentase ${data.data.persentase}` 
                            );
                        }
                        // Lakukan sesuatu dengan data respons di sini
                    })
                    .catch((error) => {
                        console.error("There was an error!", error);
                    });
            } else {
                alert("Pokemon sudah ada sebelumnya!");
            }
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    };

    const handleUpdatePokemon = async (pokemonstatus) => {
        try {
            const isPokemonExists = savedPokemons.some(
                (pokemon) => pokemon.id === pokemons.id
            );

            if (!isPokemonExists) {
                fetch("http://localhost:3000/pokes/update-pokemon", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify({
                        name: pokemonstatus,
                    }),
                })
                    .then((response) => {
                        if (!response.ok) {
                            throw new Error("Network response was not ok.");
                        }
                        return response.json(); // Mengambil data respons dalam bentuk JSON
                    })
                    .then((data) => {
                        history.push("/pokemon-me");

                        console.log("Response from server:", data);
                    })
                    .catch((error) => {
                        console.error("There was an error!", error);
                    });
            } else {
                alert("Pokemon sudah ada sebelumnya!");
            }
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    };

    const handleRilisPokemon = async (pokemonstatus) => {
        try {
            const isPokemonExists = savedPokemons.some(
                (pokemon) => pokemon.id === pokemons.id
            );

            if (!isPokemonExists) {
                fetch(
                    `http://localhost:3000/pokes/list-pokemon/${pokemonstatus}/rilis`,
                    {
                        method: "DELETE",
                        headers: {
                            "Content-Type": "application/json",
                        },
                    }
                )
                    .then((response) => {
                        if (!response.ok) {
                            throw new Error("Network response was not ok.");
                        }
                        return response.json(); // Mengambil data respons dalam bentuk JSON
                    })
                    .then((data) => {
                        if (data.data.prima) {
                            if (data.data.status === "redirect") {
                                history.push("/pokemon-me");
                            }
                        } else {
                            alert(
                                `Belum bisa rilis nih, bilangan prima ${data.data.persentase}`
                            );
                        }
                        // Lakukan sesuatu dengan data respons di sini
                    })
                    .catch((error) => {
                        console.error("There was an error!", error);
                    });
            } else {
                alert("Pokemon sudah ada sebelumnya!");
            }
        } catch (error) {
            console.error("Error fetching data:", error);
        }
    };

    useEffect(() => {
        showPokemons();
    }, [savedPokemons]);

    return (
        <>
            <Container>
                {pokemons ? (
                    <Row className="mt-3">
                        <Col
                            xs={12}
                            md={4}
                            className="text-center"
                            style={{ width: "200px", height: "200px" }}
                        >
                            <Image
                                src={pokemons.sprites?.front_default}
                                alt={pokemons.name}
                                roundedCircle
                                fluid
                            />
                            {pokemonstatus !== "global" ? (
                                <h3 className="mt-3">{pokemonstatus}</h3>
                            ) : (
                                <h3 className="mt-3">{pokemons.name}</h3>
                            )}
                        </Col>
                        <Col xs={12} md={8}>
                            <div className="mt-3">
                                <h5>Details</h5>
                                <hr />
                                <Row>
                                    {pokemons.abilities ? (
                                        pokemons.abilities.map(
                                            (ability, index) => (
                                                <Col xs={3} key={index}>
                                                    <p>
                                                        {ability.ability.name}
                                                    </p>
                                                </Col>
                                            )
                                        )
                                    ) : (
                                        <div>Loading...</div>
                                    )}
                                </Row>

                                <h5>Stats</h5>
                                <hr />
                                <Row>
                                    {pokemons.stats ? (
                                        pokemons.stats.map((stat, index) => (
                                            <Col xs={3} key={index}>
                                                <p>
                                                    {stat.stat.name} :{" "}
                                                    {stat.base_stat}
                                                </p>
                                            </Col>
                                        ))
                                    ) : (
                                        <div>Loading...</div>
                                    )}
                                </Row>

                                <h5>Type</h5>
                                <hr />
                                <Row>
                                    {pokemons.types ? (
                                        pokemons.types.map((type, index) => (
                                            <Col xs={3} key={index}>
                                                <p>{type.type.name}</p>
                                            </Col>
                                        ))
                                    ) : (
                                        <div>Loading...</div>
                                    )}
                                </Row>
                            </div>
                        </Col>
                    </Row>
                ) : (
                    <div>Loading...</div>
                )}
                {pokemonstatus === "global" ? (
                    <Button variant="primary" onClick={handleSavePokemon}>
                        Add
                    </Button>
                ) : (
                    <Button
                        variant="primary"
                        onClick={() => handleUpdatePokemon(pokemonstatus)}
                        className="mr-4"
                    >
                        Update Name
                    </Button>
                )}{" "}
                {pokemonstatus === "global" ? (
                    <div></div>
                ) : (
                    <Button
                        variant="success"
                        onClick={() => handleRilisPokemon(pokemonstatus)}
                    >
                        Rilis
                    </Button>
                )}
            </Container>
        </>
    );
};

export default PokemonScreen;
