import "./App.css";
import Container from "react-bootstrap/Container";
import { BrowserRouter as Router, Route } from "react-router-dom";
import HomeScreen from "./secreens/Home";
import PokemonScreen from "./secreens/Pokemon";
import MyPokemonScreen from "./secreens/MyPokemon";

const App = () => {
    return (
        <Router>
            <main className="py-3">
                <Container>
                    <Route path="/pokemon/:id/:status" component={PokemonScreen} />
                    <Route path="/pokemon-me" component={MyPokemonScreen} />
                    <Route path="/" component={HomeScreen} exact />
                </Container>
            </main>
        </Router>
    );
};

export default App;
